Add custom modules as individual composer projects in this directory.

* Create a directory of the module name
* Add a composer.json file with the following contents 
* Add `custom/my_module` to your composer.json file in require.

`{
   "name": "custom/my_module",
   "description": "My Module",
   "version": "1.0.0",
   "type" : "drupal-custom-modules"
 }
`
