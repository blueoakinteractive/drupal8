Add custom themes to this folder.

* Create a composer.json file with the following contents
* Create folders for each custom theme and add theme files
* Add custom/themes to your composer.json file in require.

`
{
   "name": "custom/themes",
   "description": "Custom Themes",
   "version": "1.0.0",
   "type" : "drupal-custom-themes"
 }
`
